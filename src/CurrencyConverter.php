<?php

declare(strict_types=1);

namespace Arya;

use Exception;
use JsonException;

class CurrencyConverter
{
    private ?string $apiKey = null;
    private int $maxCacheAge = 0;

    function __construct(string $apiKey)
    {
        $this->setApiKey($apiKey);
        $this->setMaxCacheAge((60 * 60 * 24 * 7));
    }

    public function getMaxCacheAge(): float|int
    {
        return $this->maxCacheAge;
    }

    public function setMaxCacheAge(float|int $maxCacheAge): void
    {
        $this->maxCacheAge = $maxCacheAge;
    }

    public function getApiKey(): ?string
    {
        return $this->apiKey;
    }

    public function setApiKey(?string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    private function getCacheFile(): string
    {
        return dirname(__FILE__) . '/../currency.rates.cache.json';
    }

    private function readCache(): array
    {
        if (file_exists($this->getCacheFile())) {
            return json_decode(file_get_contents($this->getCacheFile()), true);
        }

        return [];
    }

    private function updateCache(string $fromCurrency, string $toCurrency, array $data): array
    {
        $cache = $this->readCache();
        $result = $cache[$fromCurrency . $toCurrency] = ['timestamp' => $data['timestamp'], 'rate' => $data['rates'][$toCurrency]];
        file_put_contents($this->getCacheFile(), json_encode($cache));

        return $result;
    }

    private function getRate(string $fromCurrency, string $toCurrency): ?float
    {
        $cache = $this->readCache();
        $rates = $cache[$fromCurrency . $toCurrency] ?? null;
        $cacheAge = $rates['timestamp'] ?? null;
        $requestApi = false;

        // Re-Fetch Data?
        if (!$rates) {
            $requestApi = true;
        }
        if ($requestApi || !$cacheAge || $cacheAge < time() - ($this->getMaxCacheAge())) {
            $requestApi = true;
        }
        if ($requestApi) {
            $response = $this->fetchFromApi($fromCurrency, $toCurrency);
            $rates = $this->updateCache($fromCurrency, $toCurrency, $response);
        }

        return $rates['rate'] ?? null;
    }

    /**
     * @param string $fromCurrency
     * @param string $toCurrency
     * @return array
     * @throws JsonException
     */
    private function fetchFromApi(string $fromCurrency, string $toCurrency): array
    {
        $url = sprintf("https://api.apilayer.com/fixer/latest?base=%s&symbols=%s", $fromCurrency, $toCurrency);
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => [
                "Content-Type: text/plain",
                "apikey: " . $this->getApiKey(),
            ],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ]);
        $response = curl_exec($curl);
        curl_close($curl);

        $res = json_decode($response, true, 512, JSON_THROW_ON_ERROR);
        if (!is_array($res)) {
            throw new Exception('Result from Currency API invalid: ' . $response);
        }

        return $res;
    }

    /**
     * @param string $fromCurrency
     * @param string $toCurrency
     * @param float $amount
     * @return float|null
     * @throws Exception
     */
    public function convert(string $fromCurrency, string $toCurrency, float $amount): ?float
    {
        if (strlen($toCurrency) != 3) {
            throw new Exception("Invalid Currency Code");
        }

        $rate = $this->getRate($fromCurrency, $toCurrency);
        if (!$rate) {
            throw new Exception("Loaded rate invalid");
        }

        return $amount * $rate;
    }
}
