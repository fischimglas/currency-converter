# Currency Converter

Currency converter to Convert amount from base currency to Desired currency .
The [fixer-api](https://apilayer.com/marketplace/fixer-api) will provide the exchange rate. Exchange Rate will be
cached.

Get your API key from [fixer-api](https://apilayer.com/)

## Usage

```php
require 'CurrencyConverter.php';

$apiKey ="KDGHDHXXXXX";

$converter = new \Arya\CurrencyConverter($apiKey);
echo $converter->convert("CHF","NPR", 300);
```
