<?php

use Arya\CurrencyConverter;

require '../src/CurrencyConverter.php';

$key = 'xxx';
$converter = new  CurrencyConverter($key);

$tests = [
    ['CHF', 'NPR', 120],
    ['NPR', 'IDR', 12000],
    ['IDR', 'CHF', 120000000],
    ['INR', 'CHF', 50000],
    ['IDR', 'CHF', 220000000],
];
foreach ($tests as $item) {
    echo 'CUR ' . $item[0] . ' => ' . $item[1] . ' ' . $item[2] . ' => ' . $converter->convert($item[0], $item[1], $item[2]) . PHP_EOL;
}

